// 11.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main()
{
	int arr[5][3], i, j, check = 0;
	for (i = 0; i < 5; i++) {
		for (j = 0; j < 3; j++) {
			arr[i][j] = i - j*j;
			printf("%4d ", arr[i][j]);
			if (arr[i][j] > 0)
				check++;
		}
		printf("\n");
	}
	printf("dodatnih elementiv: %d\n", check);
    return 0;
}

