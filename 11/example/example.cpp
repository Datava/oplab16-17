﻿// example.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<time.h>
#include<conio.h>
#include<stdlib.h>
#define MAX_ROWS 50
#define MAX_COLS 50
#define MIN_VALUE -10
#define MAX_VALUE 10

int main()
{
	int matrix[MAX_ROWS][MAX_COLS];
	int rowsCount, colsCount;
	int minValue = MIN_VALUE, maxValue = MAX_VALUE;
	int summa = 0;
	int count = 0;
	int maxElement;
	int zeroInColFlag;
	printf("Rows = "); scanf_s("%d", &rowsCount);
	printf("Cols = "); scanf_s("%d", &colsCount);
	srand(time(0));
	for (int i = 0; i < rowsCount; i++)
		for (int j = 0; j < colsCount; j++)
			matrix[i][j] = minValue + rand() % (maxValue - minValue + 1);
	for (int i = 0; i < rowsCount; i++)
	{
		for (int j = 0; j < colsCount; j++)
		{
			printf("%4d", matrix[i][j]);
		}
		printf("\n");
	}
	// ���� �������� ��������
	for (int i = 0; i < rowsCount; i++)
		for (int j = 0; j < colsCount; j++)
			if (matrix[i][j] > 0)
				summa += matrix[i][j];
	printf("Summa = %d\n", summa);
	// ���������� ������� ��������, �� �� ������
	// �������� ��������
	for (int colIndex = 0; colIndex < colsCount; colIndex++)
	{
		zeroInColFlag = 1;
		for (int rowIndex = 0; rowIndex < rowsCount; rowIndex++)
		{
			if (matrix[rowIndex][colIndex] == 0)
			{
				zeroInColFlag = 0;
				break;
			}
		}
		count += zeroInColFlag;
	}
	/* ���������� �����, � ���� �������� ������������ �������
	������� ����� */
	int maxElementsArray[MAX_ROWS];
	/* ����� �� ��������������� ����� maxElement, � ������
	�� �������� maxElementsArray[rowIndex]
	*/
	for (int rowIndex = 0; rowIndex < rowsCount; rowIndex++)
	{
		maxElement = matrix[rowIndex][0];
		for (int colIndex = 1; colIndex < colsCount; colIndex++)
		{
			if (matrix[rowIndex][colIndex] > maxElement)
				maxElement = matrix[rowIndex][colIndex];
		}
		maxElementsArray[rowIndex] = maxElement;
	}
	printf("maxElementsArray = {");
	for (int index = 0; index < rowsCount; index++)
	{
		printf("%d", maxElementsArray[index]);
		if (index != rowsCount - 1)
			printf(", ");
	}
	printf("}");
	return 0;
}